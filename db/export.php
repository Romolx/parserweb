<?php
  include realpath('Class_database.php');

  function up($word) {
    return strtoupper($word);
  }

  $tables = array();

  $db = new database();

  $get_tables = $db->tableList();

  foreach ($get_tables as $key => $value) 
    foreach ($value as $k => $table) 
      $tables[] = $table; // в массив загоняем все таблицы

  if (count($tables) == 0) {
    echo 'Нечего экспортировать';
    exit;
  }

  foreach ($tables as $key => $table) {

    $db->db_table = $table;
    $columns_info = $db->getColumns(true);

    $query = $db->sql_query("SELECT * FROM `".$table."`");

    $dump_content_file .= "\n\nCREATE TABLE IF NOT EXISTS `".$table."` (\n";

    for ($i=0; $i < count($columns_info); $i++) { 

      $field    = $columns_info[$i]['Field'];
      $type     = $columns_info[$i]['Type'];
      $null     = $columns_info[$i]['Null'] == 'NO' ? 'NOT NULL' : NULL;
      $key      = $columns_info[$i]['Key'] == 'PRI' ? $uniq = $field : '';
      $default  = $columns_info[$i]['Default'] == null ? null : "DEFAULT '".$columns_info[$i]['Default']."'";
      $extra    = $columns_info[$i]['Extra'];

      if ($extra  == 'auto_increment') 
        $auto_inc = $field;
      
      if ($i+1 >= count($columns_info)) 
        if ($uniq)
          $dump_content_file .= "  "."`".$field."`"." ". up($type) ." ". up($default) ." ". up($null) ." ". up($extra) .",\n";
        else
          $dump_content_file .= "  "."`".$field."`"." ". up($type) ." ". up($default) ." ". up($null) ." ". up($extra) ."\n";
      else
        $dump_content_file .= "  "."`".$field."`"." ". up($type) ." ". up($default) ." ". up($null) ." ". up($extra) .",\n";
      
      if ($i+1 >= count($columns_info)) {
        if ($uniq)
          $dump_content_file .= "  "."UNIQUE KEY"." "."`".$uniq."`"." "."(`".$uniq."`)";  

          $dump_content_file .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        
        $db->db_table = $table;
        $columns = $db->getColumns();

        foreach ($columns as $key => $name_column) 
          $params .= "`".$name_column."`,";
        
        $modif_params = substr($params, 0, strlen($params)-1);    

        foreach ($query as $key => $array_data) {
          $data .= "(";       
          foreach ($array_data as $k => $value) {
            $data .= "'".mysql_escape_string($value)."',";
          }
          $modif_data = substr($data, 0, strlen($data)-1);
          $data = $modif_data."),\n";   
        }


        $modif_data = substr($data, 0, strlen($data)-1);   
        
        $dump_content_file .= "\n\n\n\n\n\n\n";
        $dump_content_file .= "INSERT INTO `".$table."` (".$modif_params.") VALUES\n".$modif_data.";";
        
        
        $uniq = false;        
        $params = null;
        $modif_data = NULL;
        $data = null;
        $modif_params = null;
      
      }
    }

    $fh = fopen('../../export/'.$table.'.sql', 'w');
    fwrite($fh, $dump_content_file);
    fclose($fh);

    $dump_content_file = '';
  }
echo 'Все базы успешно экспортированы!';