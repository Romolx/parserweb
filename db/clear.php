<?php 
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	require_once('config.php');

	$mysqli = new mysqli($server, $user, $password, $db);
	$mysqli->query('SET foreign_key_checks = 0');

	if ($result = $mysqli->query("SHOW TABLES"))
	    while($row = $result->fetch_array(MYSQLI_NUM))
	        $mysqli->query('DROP TABLE IF EXISTS `'.$row[0].'`');

	$mysqli->query('SET foreign_key_checks = 1');
	$mysqli->close();
	
	echo 'done';