<?php
	
	ob_start();
	header('Content-type: text/html; charset=utf-8');
	$dir    = 'parser';
	$files = scandir($dir);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="js/jquery-2.1.4.js" type="text/javascript"></script>
		<link rel="stylesheet" href="css/style.css">
		<title>Parser</title>
	</head>
	<body>
		<input type="hidden" id="secretField" value="">
		<input type="hidden" id="secretField1" value="01.01.2015">
		<input type="hidden" id="secretField2" value="01.01.2017">
		<input type="hidden" id="fullparse" value="false">
		<div class="buttons">
			<div class="select">
				<select name="sites" id="targets">
				<option value=" ">-Выберите цель-</option>
				<?php
				foreach ($files as $key) 
					if($key != '.' && $key != '..') 
						echo "<option value='" . $key . "'>" . str_replace('.php', '', $key) . "</option>";
				?>
				</select>
			</div>
			<div class = "btns" id="startparser">Запустить парсер</div>
			<div class = "btns" id="cleardb">Очистить БД</div>
			<div class = "btns" id="export">Экспортировать БД</div>
		</div>
		<div class="loader"><img src="img/load.gif"></div>
		<div class="content"></div>
		<script src="js/script.js"></script>
		
	</body>
</html>